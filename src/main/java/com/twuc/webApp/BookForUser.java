package com.twuc.webApp;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class BookForUser {

    @GetMapping(value = "/api/users/{userid}/books")
    public String getUser(@PathVariable(value = "userid") int userId) {
        return "The book for user " + userId;
    }

    @GetMapping("/segments/good")
    public String getGoodMessage(){
        return "good1";
    }

    @GetMapping("/segments/{segmentName}")
    public String getGoodMessageBypath(@PathVariable String segmentName) {
        return "good2";
    }

    @GetMapping("/users/wildcard?")
    public String getSuccessfulMessage(){
        return "success";
    }

    @GetMapping("/wildcards/*")
    public String getSuccessfulMessageWithAnything(){
        return "success hhh";
    }

    @GetMapping("/wildcards/*/hhh")
    public String getSuccessfulMessageWithAnythingAndMid(){
        return "success hhh";
    }

    @GetMapping("/wildcard/before/*/after")
    public String getSuccessfulMessageWithAnythingBetween(){
        return "success hey";
    }

    @GetMapping("/wildcard/before/**/after")
    public String getSuccessfulMessageWithAnythingBetweenAndDouble(){
        return "success hey";
    }

    @GetMapping("/wildcard/before/{[a]+}/after")
    public String getSuccessfulMessageUseRegularExpression(){
        return "Regular expression";
    }

    @GetMapping("/query")
    public  String getQueryMessage(@RequestParam String message) {
        return "Query Message";
    }







}
