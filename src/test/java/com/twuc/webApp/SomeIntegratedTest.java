package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class SomeIntegratedTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void should_return_200_status_given_request() throws Exception {
        mockMvc.perform(get("/api/users/2/books"))
                .andExpect(status().is(200))
                .andExpect(content().contentTypeCompatibleWith("text/plain"))
                .andExpect(content().string("The book for user 2"));
    }

    @Test
    public void should_return_200_status_given_get_request() throws Exception {
        mockMvc.perform(get("/api/users/2/books"))
                .andExpect(status().is(200));
    }

    @Test
    public void should_return_first_message_given_const_path() throws Exception {
        mockMvc.perform(get("/api/segments/good"))
                .andExpect(status().is(200))
                .andExpect(content().string("good1"));
    }

    @Test
    public void should_return_first_message_given_var_path() throws Exception {
        mockMvc.perform(get("/api/segments/good"))
                .andExpect(status().is(200))
                .andExpect(content().string("good1"));
    }

    @Test
    void should_return_successful_message() throws Exception {
        mockMvc.perform(get("/api/users/wildcardA"))
                .andExpect(status().is(200))
                .andExpect(content().string("success"));
    }

    @Test
    void should_return_404_status_given_null_as_wild_card() throws Exception {
        mockMvc.perform(get("/api/users/wildcard"))
                .andExpect(status().is(404));
    }

    @Test
    void should_return_succ_hhh_given_anything_wild_card() throws Exception {
        mockMvc.perform(get("/api/wildcards/anything"))
                .andExpect(status().is(200))
                .andExpect(content().string("success hhh"));
    }

    @Test
    void should_return_succ_hhh_given_anything_mid_wild_card() throws Exception {
        mockMvc.perform(get("/api/wildcards/anything/hhh"))
                .andExpect(status().is(200))
                .andExpect(content().string("success hhh"));
    }

    @Test
    void should_return_succ_hhh_given_anything_between_wild_card() throws Exception {
        mockMvc.perform(get("/api/wildcard/before/after"))
                .andExpect(status().is(404));
    }

    @Test
    void should_return_succ_hhh_given_anything_between_double_wild_card() throws Exception {
        mockMvc.perform(get("/api/wildcard/before/after"))
                .andExpect(status().is(200))
                .andExpect(content().string("success hey"));
    }

    @Test
    void should_return_succ_hhh_given_regular_expression() throws Exception {
        mockMvc.perform(get("/api/wildcard/before/aaa/after"))
                .andExpect(status().is(200))
                .andExpect(content().string("Regular expression"));
    }

    @Test
    void should_return_succ_hhh_given_QueryMessage() throws Exception {
        mockMvc.perform(get("/api/query?message=messagehhh"))
                .andExpect(status().is(200))
                .andExpect(content().string("Query Message"));
    }

    @Test
    void should_return_succ_hhh_given_QueryMessage_bad() throws Exception {
        mockMvc.perform(get("/api/query"))
                .andExpect(status().is(400));
    }

}
