package com.twuc.webApp;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;



public class BookForUserTest {
    BookForUser bookForUser = new BookForUser();
    @Test
    public void should_return_message_given_request() {
        int id = 2;
        String user = bookForUser.getUser(id);
        String except = "The book for user " + id;
        Assertions.assertEquals(except,user);
    }
}
